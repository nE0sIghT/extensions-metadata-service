# GNOME Extensions metadata service

This repository contains web microservice used by GNOME extensions website to
parse extensions metadata and validate Shell versions.  
This is pre-release prototype used in development branch of website.

# Develop inside container

It's possible to use VSCode Remote Containers (devcontainer) for fast development workspace setup.
Please look to the [`nE0sIghT/ego-devcontainer`](https://gitlab.gnome.org/nE0sIghT/ego-devcontainer) repository for instructions.
