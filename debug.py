# SPDX-License-Identifer: GPL-3.0-or-later

import os
import uvicorn

if __name__ == "__main__":
    uvicorn.run(
        "extensions_metadata.routes:app",
        host=os.getenv("EXTENSIONS_METADATA_HOST", "0.0.0.0"),
        port=int(os.getenv("EXTENSIONS_METADATA_PORT", "8085")),
        reload=True
    )
