# SPDX-License-Identifer: GPL-3.0-or-later

class InvalidShellVersion(ValueError):
    pass
