# SPDX-License-Identifer: GPL-3.0-or-later

from enum import Enum
import re
from typing import Dict, Optional, Sequence
from pydantic import BaseModel, HttpUrl, constr, conlist, validator

from .exceptions import InvalidShellVersion


def parse_version_string(version_string: str):
    prerelease_versions = {
        'alpha': -4,
        'beta': -3,
        'rc': -2,
    }
    version = version_string.split('.')
    version_parts = len(version)

    if version_parts < 1 or version_parts > 4:
        raise InvalidShellVersion("version parts must be between 1 and 4")

    try:
        major = int(version[0])
        minor = version[1] if version_parts > 1 else "-1"

        # GNOME 40+
        # https://discourse.gnome.org/t/new-gnome-versioning-scheme/4235
        if major >= 40 and minor in prerelease_versions:
            minor = prerelease_versions[minor]
        else:
            minor = int(minor)
    except ValueError as ex:
        raise InvalidShellVersion("wrong major or minor part") from ex

    point = -1
    if version_parts > 2:
        if major < 40:
            # 3.0.1, 3.1.4
            try:
                point = int(version[2])
            except ValueError as ex:
                raise InvalidShellVersion("wrong point part") from ex
    else:
        if major < 40 and (
            version_parts < 2 or (minor is not None and minor % 2 != 0)
        ):
            # Two-digit pre-40 odd versions are illegal: 3.1, 3.3
            raise InvalidShellVersion("wrong pre-40 odd version")

    return major, minor, point


class MetadataType(Enum):
    JSON = 'json'
    INI = 'ini'


class SessionMode(Enum):
    USER = 'user'
    UNLOCK_DIALOG = 'unlock-dialog'
    GDM = 'gdm'


class ShellVersion(BaseModel):
    major: int
    minor: int
    point: int


class ExtensionMetadata(BaseModel):
    uuid: constr(strip_whitespace=True, min_length=3, max_length=64, regex=r'[-a-zA-Z0-9@._]+$')
    name: constr(strip_whitespace=True, min_length=3, max_length=64)
    localized_names: Optional[Dict[str, str]]
    description: constr(strip_whitespace=True, min_length=8, max_length=2000)
    localized_description: Optional[Dict[str, str]]
    url: Optional[HttpUrl]
    shell_versions: conlist(item_type=ShellVersion, min_items=1, unique_items=True)
    session_modes: Optional[conlist(item_type=SessionMode, min_items=0, unique_items=True)]

    @validator("uuid")
    def uuid_must_not_contains_gnome_org(cls, value: str):
        if re.search(r'[.@]gnome\.org$', value) is not None:
            raise ValueError("must not ends with [.@]gnome.org")

        return value

    @validator('shell_versions', each_item=True, pre=True)
    def validate_shell_version(cls, value: str):
        if isinstance(value, str):
            major, minor, point = parse_version_string(value)
            return ShellVersion(major=major, minor=minor, point=point)

        if not isinstance(value, dict):
            raise ValueError("Wrong shell version value type")

        for key in ("major", "minor", "point"):
            if not isinstance(value.get(key), int):
                raise ValueError(f"{key} is not int")

        return value

    @validator('session_modes', each_item=False, pre=True)
    def validate_session_modes(cls, value: Sequence[SessionMode]):
        if not value:
            return [SessionMode.USER]

        return value
