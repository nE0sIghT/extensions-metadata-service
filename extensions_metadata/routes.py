# SPDX-License-Identifer: GPL-3.0-or-later

import json
import os
from typing import Any, Union

from content_size_limit_asgi import ContentSizeLimitMiddleware
from gi.repository import GLib
from fastapi import APIRouter, FastAPI, UploadFile, status
from fastapi.responses import JSONResponse, RedirectResponse
from pydantic import ValidationError
from starlette.responses import Response

from extensions_metadata.exceptions import InvalidShellVersion

from .models import ExtensionMetadata, MetadataType, ShellVersion, parse_version_string

INI_GROUP = 'GNOME Extension'

app = FastAPI(
    title="GNOME Extensions metadata service",
    version="1.alpha"
)
app.add_middleware(
    ContentSizeLimitMiddleware,
    max_content_size=int(
        os.getenv("EXTENSIONS_METADATA_MAX_REQUEST_SIZE", str(5 * 1024 * 1024))
    )
)
v1 = APIRouter(prefix='/api/v1')


async def get_keyfile_value(keyfile: GLib.KeyFile, group: str, name: str, default: Any = None, method_name: str = 'get_string'):
    method = getattr(keyfile, method_name)
    try:
        return method(group, name)
    except GLib.GError:
        return default


@app.exception_handler(ValidationError)
async def validation_exception_handler(request, exc: ValidationError):
    return JSONResponse(
        {
            'errors': exc.errors(),
        },
        status_code=status.HTTP_422_UNPROCESSABLE_ENTITY
    )


@app.exception_handler(GLib.GError)
async def validation_exception_handler(request, exc: GLib.GError):
    return JSONResponse(
        {
            'detail': exc.message,
        },
        status_code=status.HTTP_422_UNPROCESSABLE_ENTITY
    )


@app.get("/")
async def root():
    return RedirectResponse("/docs")


@v1.post("/metadata/parse", response_model=ExtensionMetadata)
async def parse_metadata(file: UploadFile, type: MetadataType) -> Union[ExtensionMetadata, Response]:
    if type == MetadataType.JSON:
        try:
            metadata = json.load(file.file)
        except json.JSONDecodeError:
            return JSONResponse(
                {
                    'detail': 'Invalid JSON file',
                },
                status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
            )

        return ExtensionMetadata(
            uuid=metadata.get('uuid'),
            name=metadata.get('name'),
            localized_names=None,
            description=metadata.get('description'),
            localized_description=None,
            url=metadata.get('url'),
            shell_versions=metadata.get('shell-version'),
            session_modes=metadata.get('session-modes'),
        )
    else:
        keyfile = GLib.KeyFile.new()
        try:
            GLib.KeyFile.load_from_bytes(
                keyfile, GLib.Bytes.new(file.file.read()), GLib.KeyFileFlags.KEEP_TRANSLATIONS
            )
        except GLib.GError as ex:
            return JSONResponse(
                {
                    'detail': ex.message,
                },
                status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
            )

        return ExtensionMetadata(
            uuid=keyfile.get_string(INI_GROUP, 'Uuid'),
            name=keyfile.get_string(INI_GROUP, 'Name'),
            localized_names=None,
            description=(
                keyfile.get_string(INI_GROUP, 'Description') or keyfile.get_string(INI_GROUP, 'Comment')
            ),
            localized_description=None,
            url=await get_keyfile_value(keyfile, INI_GROUP, "URL", None),
            shell_versions=keyfile.get_string_list(INI_GROUP, 'ShellVersion'),
            session_modes=await get_keyfile_value(keyfile, INI_GROUP, "SessionModes", None, "get_string_list")
        )


@v1.post("/shell-version/parse", response_model=ShellVersion)
async def parse_shell_version(version: str) -> Union[ShellVersion, Response]:
    try:
        major, minor, point = parse_version_string(version)
    except InvalidShellVersion:
        return JSONResponse(
            {
                'detail': "Invalid Shell version",
            },
            status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
        )

    return ShellVersion(major=major, minor=minor, point=point)

app.include_router(v1)
